
const user = {
    name: "Vika",
    surname: "Kishchenko",
    age: "19",
    hobby: "dance",
    address: {
        contry: "Ukraine",
        city: "Kyiv",
        street: "Abcd",
        number: "123"
    }
}

console.log(user);

const cloneObj = function (obj, clone) { 
    clone = JSON.parse(JSON.stringify(obj));
    return clone;
}
    
cloneUser = cloneObj(user);
cloneUser.address.city = "Korsun";
console.log(cloneUser);
